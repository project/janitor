/**
 * @file
 * JS functions to support theming in the Janitor theme.
 */

/*
 * Janitor Base Theme : Anton Boshoff
 * ==== INDEX
 * Add user agent.
 * Global variables.
 * Sticky header.
 * Navigation width.
 * Navigation overflow.
 * Toggle navigation.
 * Expand navigation.
 * Collapse navigation.
 * Mobile toggle expandable items.
 * Collapse mobile expandable items.
 * Add scroll to navigation.
 * Accordion.
 * Tabber.
 * Disable middle mouse click.
 */


// ==== NAMESPACE.
window.app = {};

// ==== TRIGGER ON DOCUMENT READY.
jQuery(function ($) {

  'use strict';

  app.go.addUserAgent();
  app.go.globalVar();
  app.go.stickyHeader();
  app.go.navWidth();
  app.go.navOverflow();
  app.go.toggleNav();
  app.go.mobileMenuItemsToggle();
  app.go.initAccordion();
  app.go.initTabber();
  app.go.disableMiddleMouse();

  var lazyLayout = _.debounce(updateResizeDebounce, 10);
  app.go.win.resize(lazyLayout);

  var rThrottled = _.throttle(updateResizeThrottle, 10);
  app.go.win.resize(rThrottled);

  var sThrottled = _.throttle(updateScrollThrottle, 10);
  app.go.win.scroll(sThrottled);

});

// ==== TRIGGER AFTER PAGE LOAD.
jQuery(window).on('load', function () {

  'use strict';

  // Variables.
  app.go.win = jQuery(window);

  // Dynamic values.
  app.go.winWidth = app.go.win.width();


});

// ==== TRIGGER ON RESIZE ( DEBOUNCE ).
function updateResizeDebounce() {

  'use strict';

}

// ==== TRIGGER ON RESIZE ( THROTTLE ).
function updateResizeThrottle() {

  'use strict';

  // Dynamic values.
  app.go.winWidth = app.go.win.width();

  app.go.navOverflow();

  if ( app.go.winWidth < 960 || app.go.isTablet === true ) {
    app.go.isMobile = true;
  }
  else {
    app.go.isMobile = false;
  }

  app.go.navWidth();

  if (app.go.isMobile === false) {
    app.go.expandNav();
    app.go.mobileMenuItemsCollapse();
    if (jQuery('.header-fixed').length === 0 && jQuery('.header-sticky').length === 0) {
      app.go.body.css({
        overflowY: 'visible',
        position: 'relative'
      });
    }
    if ( app.go.nav.hasClass('menu-full') ) {
      app.go.nav.css({
        height: 'auto'
      });
    } else {
      app.go.nav.css({
        height: ''
      });
    }
  }
  else {
    app.go.collapseNav();
    if (app.go.subInit !== true) {
      app.go.mobileMenuItemsToggle();
    }
  }

}

// ==== TRIGGER ON SCROLL ( THROTTLE ).
function updateScrollThrottle() {

  'use strict';

  // Dynamic values.
  app.go.winTop = app.go.win.scrollTop();

  app.go.stickyHeader();

}


// ==== FUNCTIONS.
app.go = {

  // Add user agent.
  addUserAgent: function () {

    'use strict';

    app.go.html = jQuery('html');
    var userAgent = navigator.userAgent;

    app.go.html.attr('data-browser', userAgent);

  },

  // Global variables.
  globalVar: function () {

    'use strict';

    // Variables.
    app.go.win = jQuery(window);
    app.go.body = jQuery('body');
    app.go.nav = jQuery('nav#block-janitor-main-menu');

    // Dynamic values.
    app.go.winTop = app.go.win.scrollTop();
    app.go.winWidth = app.go.win.width();
    app.go.winHeight = app.go.win.height();

    // get user agent from html data attr
    var fullBrowser = app.go.html.data('browser');

    // check if is tablet
    if ( fullBrowser.toLowerCase().indexOf('ipad') >= 0 || fullBrowser.toLowerCase().indexOf('android') >= 0 ) {
      app.go.isTablet = true;
    } else {
      app.go.isTablet = false;
    }

    if ( app.go.winWidth < 960 || app.go.isTablet === true ) {
      app.go.isMobile = true;
    }
    else {
      app.go.isMobile = false;
    }

  },

  // Sticky header.
  stickyHeader: function () {

    'use strict';

    var header = jQuery('header#page-header');

    if (header.hasClass('header-sticky')) {

      var elementB = jQuery('.header-sticky-before');
      var elementA = jQuery('.header-sticky-after');
      var heightB = elementB.outerHeight();

      if (app.go.winTop > heightB) {
        header.addClass('stick');
        elementA.addClass('header-stick');
      }
      else {
        header.removeClass('stick');
        elementA.removeClass('header-stick');
      }

    } return false;

  },

  // Navigation width.
  navWidth: function () {

    'use strict';

    var containerWidth = jQuery('.jcontainer').width();
    var marginValue = (app.go.winWidth - containerWidth) / 2;

    if (app.go.isMobile) {
      app.go.nav.css({
        width: app.go.winWidth,
        marginLeft: -marginValue,
        marginRight: -marginValue
      });
    }
    else {
      if ( app.go.nav.hasClass('menu-full') ) {
        app.go.nav.css({
          width: '100%',
          marginLeft: 0,
          marginRight: 0
        });
      } else {
        app.go.nav.css({
          width: 'auto',
          marginLeft: 0,
          marginRight: 0
        });
      }
    }

  },

  // Navigation overflow.
  navOverflow: function () {

    'use strict';

    // get all parent-0
    var item1 = app.go.nav.find('.parent-0');

    // for each item
    item1.each(function() {

      // this variables
      var self = jQuery(this);
      var wrap2 = self.find('.parent-1').parent('ul');
      var wrap3 = self.find('.parent-2').parent('ul');

      // get each left position relative to window
      var posX = self.offset().left;

      // calculate remaining right space
      var rightSpace = app.go.winWidth - posX;

      // get widths of the first wrapper ul
      var width2 = wrap2.outerWidth();

      if ( rightSpace < (width2 * 2) ) {
        wrap3.addClass('wrapper-3-fit');
        if ( rightSpace < width2 ) {
          wrap2.addClass('wrapper-2-fit');
        }
        else {
          wrap2.removeClass('wrapper-2-fit');
        }
      }
      else {
        wrap2.removeClass('wrapper-2-fit');
        wrap3.removeClass('wrapper-3-fit');
      }

    });

  },

  // Toggle navigation.
  toggleNav: function () {

    'use strict';

    var toggler = jQuery('.toggler');

    toggler.on('click', function () {

      if (app.go.isMobile) {
        if (app.go.isActive) {
          app.go.collapseNav();
        }
        else {
          app.go.expandNav();
        }
      } return false;

    });

  },

  // Expand navigation.
  expandNav: function () {

    'use strict';

    if (!app.go.isActive) {

      app.go.nav.show();
      jQuery('.toggler').addClass('open');
      if (jQuery('.header-fixed').length === 0 && jQuery('.header-sticky').length === 0) {
        app.go.body.css({
          overflowY: 'hidden',
          position: 'fixed'
        });
      }
      jQuery('.blackout').show();
    }
    app.go.isActive = true;

    app.go.navScroll();

  },

  // Collapse navigation.
  collapseNav: function () {

    'use strict';

    if (app.go.isActive) {
      app.go.nav.hide();
      jQuery('.toggler').removeClass('open');
      if (jQuery('.header-fixed').length === 0 && jQuery('.header-sticky').length === 0) {
        app.go.body.css({
          overflowY: 'visible',
          position: 'relative'
        });
      }
      jQuery('.blackout').hide();
    }
    app.go.isActive = false;

  },

  // Mobile toggle expandable items.
  mobileMenuItemsToggle: function () {

    'use strict';

    var menuItemSub = app.go.nav.find('li > sub');

    if (app.go.isMobile) {
      app.go.subInit = true;
      menuItemSub.on('click', function () {

        var self = jQuery(this);

        self.children('i').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down').toggleClass('active');
        self.siblings('ul').toggleClass('active');

        self.closest('li').toggleClass('active-li');

      });
    }

  },

  // Collapse mobile expandable items.
  mobileMenuItemsCollapse: function () {

    'use strict';

    var menuItemSub = app.go.nav.find('li > sub');

    menuItemSub.children('i').removeClass('fa-chevron-up').addClass('fa-chevron-down').removeClass('active');
    menuItemSub.siblings('ul').removeClass('active');

    menuItemSub.parent('li').removeClass('active-li');

  },

  // Add scroll to navigation.
  navScroll: function () {

    'use strict';

    if (app.go.isActive && app.go.isMobile) {

      var topHeight = 120;

      app.go.nav.css({
        height: app.go.winHeight - topHeight
      });

    }

  },

  // Accordion.
  initAccordion: function () {

    'use strict';

    // variables.
    var accordion = jQuery('.jaccordion');
    var bar = accordion.find('.itemBar');
    var visible = accordion.find('.is-visible');

    // Add classes to identify and separate nested accordion functionality
    accordion.each(function () {
      var thisAccordion = jQuery(this);
      var digit = thisAccordion.parents('.jaccordion').length;

      thisAccordion.attr('data-accordion', digit);
      thisAccordion.find('> li').attr('data-li', digit);
      thisAccordion.find('.itemBar').attr('data-bar', digit);
      thisAccordion.find('.itemContent').attr('data-content', digit);
    });

    // show items with .is-visible class
    visible.each(function () {

      var thisVisible = jQuery(this);
      var getValue = thisVisible.attr('data-li');

      thisVisible.find('.itemBar')
        .addClass('is-expanded')
        .siblings('.itemContent[data-content="' + getValue + '"]').show().css('opacity', 1);

    });

    // On click.
    bar.on('click', function () {

      // in scope variables
      var thisBar = jQuery(this);
      var thisContent = thisBar.siblings('.itemContent');
      var thisAccordion = thisBar.closest('.jaccordion');

      // establish which accordion
      var dataValue = thisBar.attr('data-bar');

      // out of scope "sibling" bars based on dataValue
      var siblingBars = thisBar.closest('.jaccordion').find('.itemBar[data-bar="' + dataValue + '"]').not(this);

      // if items independent from siblings.
      if ( thisAccordion.hasClass('is-independent') ) {
        var indi = true;
      } else {
        var indi = false;
      }

      function collapseSiblings() {
        siblingBars.each(function () {
          
          var siblingBar = jQuery(this);
          var siblingContent = siblingBar.siblings('.itemContent');

          // check if any siblings are expanded
          if ( siblingBar.hasClass('is-expanded') ) {
            
            siblingBar.removeClass('is-expanded');

            siblingContent.animate({opacity: 0}, function () {
              jQuery(this).slideUp();
            });

          }

        });
      }

      if ( indi === false ) {
        collapseSiblings();
      } else {
        // do nothing
      }

      // check if expanded
      if ( thisBar.hasClass('is-expanded') ) {

        // remove expanded class to thisBar
        thisBar.removeClass('is-expanded');

        // hide sibling content
        thisContent.animate({opacity: 0}, function () {
          jQuery(this).slideUp();
        });

      } else {

        // add expanded class to thisBar
        thisBar.addClass('is-expanded');

        // show sibling content
        thisContent.slideDown(function () {
          jQuery(this).animate({opacity: 1});
        });

      }
    });

  },

  // Tabber.
  initTabber: function () {

    'use strict';

    jQuery('ul.jtabber>li').on('click', function () {
      var self = jQuery(this);
      var dataContent = self.attr('data-contents');
      var tabberContent = self.parent('ul').siblings('.jtabberContent');

      if (!self.parent().hasClass('open')) {
        self.siblings().removeClass('open');
        self.addClass('open');

        tabberContent.children('li[data-tab!=' + dataContent + ']').hide();
        tabberContent.children('li[data-tab=' + dataContent + ']').show();
      }
    });

  },

  // Disable middle mouse click.
  disableMiddleMouse: function () {

    'use strict';

    app.go.body.on('mousedown', function (e) {

      if (e.which === 2) {
        e.preventDefault();
      }

    });

  }

};
