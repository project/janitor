CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Janitor is a small mobile first front-end framework. Uncomplicated build for
easy integration.

### Features
 * Normalize.css to render elements more consistently and in line with modern
 standards. See https://necolas.github.io/normalize.css/
 * Underscore.js provides helper functions. See http://underscorejs.org/
 * 11 Regions
 * 12 Column grid system
 * Font Awesome support. See http://fontawesome.io/
 * Built in functions:
   * Burger menu
   * Static header
   * Sticky header
   * Fixed header
   * Accordion
   * Tabber
   * Equal heights
   * Sidebar height
 * Theme settings:
   * Header style: **Relative**, **sticky** or **fixed**.
   * Container style: **Adaptive**, **fluid** or **set**.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal theme. See
 https://www.drupal.org/docs/8/extending-drupal/installing-themes for further
 information.

CONFIGURATION
-------------

The theme has only 2 configuring options. Header style and container style.
 * After installation go to Manage » Appearance » Settings (Janitor)
 * Header style:
   * **Relative (Default)**: No specific placement. Scrolls normally with user
   interaction relative to the design.
   * **Fixed**: From the initial window load the header is fixed to the top of
   the browser window.
   * **Sticky**: Use this style if your header is not positioned at the top of
   your design and need it to stick to the browser window when it "touches"
   the top browser edge when user scrolls.
 * Container style:
   * **Adaptive (Default)**: Breakpoints/Media queries has been set for
   specific device sizes targeting feature phones, smart devices, tablets,
   desktop screens and large monitors.
   * **Fluid**: This style is for edge to edge sites. Fluid scaling from the
   left to right edge of the browser window.
   * **Set**: Similar to the Fluid style with the only difference being the
   max-width of 960px set on the container.

MAINTAINERS
-----------

Current maintainers:
 * Anton Boshoff (aboshoff) - https://www.drupal.org/user/3204203

This project has been sponsored by:
 * ROGERWILCO
   An award winning digital marketing agency that helps companies attract and
   retain customers all over the globe.
